import glob
import os
import shutil
from fnmatch import fnmatch
from os import path
from typing import List

from jinja2 import Environment, PackageLoader
from sphinx.util.osutil import FileAvoidWrite

PY_SUFFIXES = ('.py', '.pyx', 'pyw')


class Node(object):
    def __init__(self, directory: str, exclude: List[str]):
        """获取文件树，并排除用户指定过滤的部分。

        关于文件结构的过滤功能都写在了这个文件夹里，包括以下内容：

        1. 对于非 ``python package`` 的部分全部跳过；
        #. 跳过由 ``.`` 和 ``_`` 开头的文件和文件夹，不过保留 ``__*__`` 格式的文件和文件夹；
        #. 跳过 ``__pycache__`` 文件夹；
        #. 跳过用户指定要排除的文件夹。

        Args:
            directory: 需要计算文件树的文件夹。
            exclude: 需要排除的模式，采用标准库的fnmatch进行匹配

        Returns:
            整理后的文件树
        """
        self.directory = path.abspath(directory)
        children = glob.glob(path.join(self.directory, '*'))

        for file_path in children.copy():  # 这里要采用复制，因为遍历过程的原位删除会改变其遍历过程
            dir_name, file_name = path.split(file_path)
            base_name, ext_name = path.splitext(file_name)

            # 删去由 ``.`` 和 ``_`` 开头的文件和文件夹，不过对于 ``__*__`` 格式的不进行操作
            if base_name.startswith(('.', '_')) and not (base_name.startswith('__') and base_name.endswith('__')):
                children.remove(file_path)
                continue

            # 删去用户指定排除的文件和文件夹
            if any(fnmatch(file_path, pattern) for pattern in exclude):
                children.remove(file_path)
                continue

            # 删去 ``__pycache__`` 文件夹
            if path.isdir(file_path) and base_name == '__pycache__':
                children.remove(file_path)
                continue

        # 计算文件夹下的所有文件，区分 ``python`` 文件、其他文件、模块文件三类文件。
        # 所有的文件都是通过全路径进行表示的。
        # ``__init__.py`` 比较特殊，不在这里进行统计，直接排除掉了，如果 ``node.is_package`` 为真，则说明有该文件
        self.files = [child for child in children if path.isfile(child)]
        self.python_files = [file for file in self.files if path.splitext(file)[1] in PY_SUFFIXES]
        self.other_files = [file for file in self.files if file not in self.python_files]
        self.modules = [file for file in self.python_files if path.split(file)[1] != '__init__.py']

        # 排除非 python package 的内容
        self.nodes = [Node(child, exclude) for child in children if path.isdir(child)]
        self.nodes = [node for node in self.nodes if node.is_package]

        # 指明是否包括了一个 ``__init__.py`` ，用于过滤非python包的文件夹
        self.is_package = '__init__.py' in [path.split(file)[1] for file in self.python_files]


class RstGenerator(object):
    def __init__(self):
        self.env = Environment(loader=PackageLoader('pyminer2', 'dev/doc/template'))
        self.module_template = self.env.get_template('module.rst_t')
        self.package_template = self.env.get_template('package.rst_t')

    def generate_rst(self, tree: Node, dst_dir: str, package_name: str):
        """根据 ``python`` 文件结构生成 ``rst`` 文件结构。

        #. 对于不存在的文件，直接生成；
        #. 对于已存在的文件，判断是否更新，如果更新则覆写，否则不改变，以免触发 ``sphinx`` 的更新机制；
        #. 对于多余的文件，删除。

        本方法首先根据 ``python`` 文件结构生成 ``rst`` 文件结构，
        然后再将其余文件进行拷贝。
        因此，如果您本方法生成的 ``*.rst`` 不满意的话，可以采用与 ``python`` 文件同名但不同后缀的文件覆写自动生成的文件。

        例如：

        .. code-block::

            package
              |- __init__.py
              |- __init__.rst

        则 ``__init__.py`` 的内容会被弃用，而 ``__init__.rst`` 的内容会被拷贝过去。

        TODO 应删除多余的文件

        Args:
            tree: 源文件，是一个 ``Node`` 节点。
            dst_dir: 生成的 ``rst`` 文件结构。
            package_name: 用于写入模版中的 ``package_name`` 。

        Returns:
            暂无返回值

        Notes:
            本模块区别于 ``shpinx-apidoc`` 的将所有文件放在一个文件夹的方案，
            采用了层级的方案进行 ``rst`` 文件结构的组织。
            这样生成的内容更直观，且可以包含非 ``*.py`` 文件。

            关于 ``__init__.py`` 模块，在文件结构上本模块将其与其他 ``python`` 模块一致考虑，
            不过内容采用了 ``package_template`` 而非 ``module_template`` 进行渲染。

        """
        path.exists(dst_dir) or os.mkdir(dst_dir)
        dst_dir = path.abspath(dst_dir)

        # 将所有的 ``python`` 模块按 ``rst_t`` 模板进行渲染
        for module in tree.modules:
            module = path.split(module)[1]
            module = path.splitext(module)[0]
            content = self.module_template.render(pkgname=f'{package_name}.{module}')
            dst_path = path.join(dst_dir, f'{module}.rst')
            with FileAvoidWrite(dst_path) as f:
                f.write(content)

        # 将所有的 ``sub_package`` 子包进行递归处理
        for package in tree.nodes:
            sub_package_name = path.split(package.directory)[1]
            package_name_to_render = f'{package_name}.{sub_package_name}'
            dst = path.join(dst_dir, sub_package_name)
            self.generate_rst(package, dst, package_name_to_render)

        # 将 ``__init__.py`` 进行单独处理
        # 区别于其他的模块， ``__init__.py`` 相当于是包的主页，因此采用了包的模板。
        dst = path.join(dst_dir, '__init__.rst')
        sub_packages = [path.split(node.directory)[1] for node in tree.nodes]
        sub_modules = [path.split(path.splitext(module)[0])[1] for module in tree.modules]
        content = self.package_template.render(
            pkgname=package_name, subpackages=sub_packages, submodules=sub_modules)
        with FileAvoidWrite(dst) as f:
            f.write(content)

        # 将所有的非 ``python`` 文件执行简单的拷贝
        # 应当注意其余文件与 ``python`` 文件的生成顺序，将其余文件放在最后，
        # 可以使其覆写根据 ``python`` 文件自动生成的 ``*.rst`` 文件。
        for file in tree.other_files:
            dst_path = path.join(dst_dir, path.split(file)[1])
            # TODO 应检测是否可以不用写入，以免触发
            shutil.copy(file, dst_path)


