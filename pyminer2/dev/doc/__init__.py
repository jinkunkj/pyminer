"""
这个包用于进行文档的编译工作，是 ``sphinx-apidoc`` 的替代品，其主要思路如下：

#. 将 ``*.py`` 文件按照 ``jinja`` 模板生成 ``*.rst`` 文件；
#. 将 ``*.rst`` 文件采用 ``sphinx`` 生成 ``*.html`` 文件。

由于暂时仅作为 ``pyminer`` 的内部工具，暂不考虑复用性。

定义了两个结构：

#. ``Node`` ，用于进行python文件夹下的文件结构的遍历；
#. ``RstGenerator`` ，用于根据python文件结构生成rst文件结构。

TODO 具体的使用说明有待补全

"""

from ._apidoc import Node, RstGenerator

__all__ = ['Node', 'RstGenerator']
