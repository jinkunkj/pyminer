<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>PMVariableTreeWidget</name>
    <message>
        <location filename="main.py" line="107"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="main.py" line="109"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="main.py" line="111"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="main.py" line="113"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="main.py" line="131"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="main.py" line="135"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="main.py" line="136"/>
        <source>Open Variable</source>
        <translation>打开变量</translation>
    </message>
    <message>
        <location filename="main.py" line="138"/>
        <source>Quick Save to Workdir</source>
        <translation>快速保存至工作目录</translation>
    </message>
    <message>
        <location filename="main.py" line="140"/>
        <source>Save..</source>
        <translation>保存..</translation>
    </message>
    <message>
        <location filename="main.py" line="144"/>
        <source>PyMiner Data(.pmd)</source>
        <translation>PyMiner数据(.pmd)</translation>
    </message>
    <message>
        <location filename="main.py" line="145"/>
        <source>PyMiner Data(*.pmd)</source>
        <translation>PyMiner数据(*.pmd)</translation>
    </message>
    <message>
        <location filename="main.py" line="146"/>
        <source>Pickle(.pkl)</source>
        <translation>Pickle(.pkl)</translation>
    </message>
    <message>
        <location filename="main.py" line="147"/>
        <source>Pickle File(*.pkl)</source>
        <translation>Pickle文件(*.pkl)</translation>
    </message>
    <message>
        <location filename="main.py" line="149"/>
        <source>Excel Table(.xls/.xlsx)</source>
        <translation>Excel表格(.xls/.xlsx)</translation>
    </message>
    <message>
        <location filename="main.py" line="150"/>
        <source>Excel (*.xls *.xlsx)</source>
        <translation>Excel(*.xls *.xlsx)</translation>
    </message>
    <message>
        <location filename="main.py" line="152"/>
        <source>Text File (.csv)</source>
        <translation>文本文件 (.csv)</translation>
    </message>
    <message>
        <location filename="main.py" line="153"/>
        <source>CSV File(*.csv)</source>
        <translation>CSV文件(*.csv)</translation>
    </message>
    <message>
        <location filename="main.py" line="156"/>
        <source>Numpy Matrix</source>
        <translation>Numpy矩阵</translation>
    </message>
    <message>
        <location filename="main.py" line="160"/>
        <source>Save Workspace</source>
        <translation>保存工作区间</translation>
    </message>
    <message>
        <location filename="main.py" line="256"/>
        <source>Choose File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="main.py" line="292"/>
        <source>Variable %s is corrupted.</source>
        <translation>变量%s已损坏.</translation>
    </message>
    <message>
        <location filename="main.py" line="278"/>
        <source>Variable Name</source>
        <translation>变量名</translation>
    </message>
    <message>
        <location filename="main.py" line="278"/>
        <source>Please Input Variable Name:</source>
        <translation>请输入变量名:</translation>
    </message>
    <message>
        <location filename="main.py" line="294"/>
        <source>Cannot Load this type of file:%s</source>
        <translation>不能加载此类型的文件:%s</translation>
    </message>
    <message>
        <location filename="main.py" line="300"/>
        <source>Error Occurs In Loading</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.py" line="342"/>
        <source>Choose File Name To Save</source>
        <translation>选择要保存的文件名</translation>
    </message>
    <message>
        <location filename="main.py" line="323"/>
        <source>File %s cannot be saved as its extension name is not supported</source>
        <translation>扩展名不支持，文件%s不能保存</translation>
    </message>
</context>
<context>
    <name>PMWorkspaceInspectWidget</name>
    <message>
        <location filename="main.py" line="537"/>
        <source>delete variable %s</source>
        <translation>删除变量%s</translation>
    </message>
</context>
<context>
    <name>app</name>
    <message>
        <location filename="main.py" line="64"/>
        <source>Variable Viewer</source>
        <translation>查看变量</translation>
    </message>
</context>
</TS>
