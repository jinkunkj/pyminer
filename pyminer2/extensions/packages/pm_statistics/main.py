import logging
from typing import TYPE_CHECKING, Callable
from qtpy.QtCore import Signal
from qtpy.QtWidgets import QApplication
from pmgwidgets import PMGToolBar, create_icon
from pyminer2.extensions.extensionlib import BaseExtension, BaseInterface

from .stat_desc import StatDescForm

logger = logging.getLogger(__name__)

if TYPE_CHECKING:
    from pyminer2.extensions.extensionlib import extension_lib


class PMDrawingsToolBar(PMGToolBar):
    drawing_item_double_clicked_signal: 'Signal' = Signal(str)
    extension_lib: 'extension_lib' = None
    variable = None

    def __init__(self):
        super().__init__()
        self.add_tool_button('button_stat_desc', '描述统计',
                             create_icon(':/color/source/theme/color/icons/data_desc.svg'))
        self.addSeparator()
        self.add_tool_button('button_new_script', '分布分析',
                             create_icon(':/color/source/theme/color/icons/distribution.svg'))
        self.add_tool_button('button_new_script', '缺失值分析',
                             create_icon(':/color/source/theme/color/icons/data_join.svg'))
        self.addSeparator()
        self.add_tool_button('button_new_script', '参数估计',
                             create_icon(':/color/source/theme/color/icons/canshu.svg'))
        self.add_tool_button('button_new_script', '假设检验',
                             create_icon(':/color/source/theme/color/icons/jiashe.svg'))
        self.addSeparator()
        self.add_tool_button('button_new_script', '回归',
                             create_icon(':/color/source/theme/color/icons/regression.svg'))
        self.add_tool_button('button_new_script', '分类',
                             create_icon(':/color/source/theme/color/icons/Classification.svg'))
        self.addSeparator()
        self.add_tool_button('button_new_script', '多元分析',
                             create_icon(':/color/source/theme/color/icons/duoyuan.svg'))
        self.add_tool_button('button_new_script', '时间序列分析',
                             create_icon(':/color/source/theme/color/icons/time_series.svg'))
        self.add_tool_button('button_new_script', '生存分析',
                             create_icon(':/color/source/theme/color/icons/shengcunfenxi.svg'))
        self.add_tool_button('button_new_script', '方差分析',
                             create_icon(':/color/source/theme/color/icons/anova.svg'))

        self.addSeparator()

    def on_data_selected(self, data_name: str):
        """
        当变量树中的数据被单击选中时，调用这个方法。
        """
        self.current_var_name = data_name
        self.variable = self.extension_lib.get_var(data_name)
        logger.info('Variable clicked. Name is \'' + data_name + '\' , value is ' + repr(self.variable))

    def on_data_modified(self, var_name: str, variable: object, data_source: str):
        """
        在数据被修改时，调用这个方法。
        """
        pass

    def bind_events(self):
        """
        绑定事件。这个将在界面加载完成之后被调用。
        """

        self.get_control_widget('button_stat_desc').clicked.connect(self.show_stat_desc)

    def show_stat_desc(self):
        self.stat_desc = StatDescForm()
        self.stat_desc.current_dataset_name = self.current_var_name
        self.stat_desc.current_dataset = self.variable
        self.stat_desc.current_dataset_columns = self.variable.columns
        self.stat_desc.show()


class Extension(BaseExtension):
    if TYPE_CHECKING:
        interface: 'DrawingsInterface' = None
        widget: 'PMDrawingsToolBar' = None
        extension_lib: 'extension_lib' = None

    def on_loading(self):
        self.extension_lib.Program.add_translation('zh_CN', {'Drawings': '绘图', 'Variable Selected': '选择的变量',
                                                             'No Variable': '尚未选择变量'})

    def on_load(self):
        statistics_toolbar: 'PMDrawingsToolBar' = self.widgets['PMDrawingsToolBar']
        statistics_toolbar.extension_lib = self.extension_lib
        self.statistics_toolbar = statistics_toolbar
        self.interface.drawing_item_double_clicked_signal = statistics_toolbar.drawing_item_double_clicked_signal

        self.interface.drawing_item_double_clicked_signal.connect(self.interface.on_clicked)
        self.interface.statistics_toolbar = statistics_toolbar

        self.extension_lib.Data.add_data_changed_callback(statistics_toolbar.on_data_modified)
        self.extension_lib.Signal.get_widgets_ready_signal().connect(self.bind_events)

    def bind_events(self):
        workspace_interface = self.extension_lib.get_interface('workspace_inspector')
        workspace_interface.add_select_data_callback(self.statistics_toolbar.on_data_selected)


class StatisticsInterface(BaseInterface):
    drawing_item_double_clicked_signal: 'Signal' = None
    drawings_toolbar: 'PMDrawingsToolBar' = None

    def on_clicked(self, name: str):
        pass

    def add_graph_button(self, name: str, text: str, icon_path: str, callback: Callable, hint: str = ''):
        """
        添加一个绘图按钮。name表示按钮的名称,text表示按钮的文字，icon_path表示按钮的图标路径，callback表示按钮的回调函数
        hint表示的就是按钮鼠标悬浮时候的提示文字。
        例如：
        extension_lib.get_interface('drawings_toolbar').add_graph_button('aaaaaa','hahahaahahah',
                                                                         ':/pyqt/source/images/lc_searchdialog.png',lambda :print('123123123'))
        """
        self.drawings_toolbar.add_toolbox_widget(name, text, icon_path, hint, refresh=True)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    sys.exit(app.exec())
