<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="zh_CN" sourcelanguage="en">
<context>
    <name>PMDrawingsToolBar</name>
    <message>
        <location filename="../applications_toolbar.py" line="187"/>
        <source>&#xe5;&#xba;&#x94;&#xe7;&#x94;&#xa8;&#xe8;&#xae;&#xbe;&#xe8;&#xae;&#xa1;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="190"/>
        <source>&#xe5;&#xba;&#x94;&#xe7;&#x94;&#xa8;&#xe5;&#xbc;&#x80;&#xe5;&#x8f;&#x91;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="194"/>
        <source>&#xe5;&#xba;&#x94;&#xe7;&#x94;&#xa8;&#xe5;&#x8f;&#x91;&#xe5;&#xb8;&#x83;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="198"/>
        <source>&#xe8;&#x8e;&#xb7;&#xe5;&#x8f;&#x96;&#xe5;&#xba;&#x94;&#xe7;&#x94;&#xa8;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PMProcessConsoleTabWidget</name>
    <message>
        <location filename="../process_monitor.py" line="50"/>
        <source>Terminate</source>
        <translation type="unfinished">终止</translation>
    </message>
    <message>
        <location filename="../process_monitor.py" line="50"/>
        <source>Process is running, Would you like to terminate this process?</source>
        <translation type="unfinished">进程正在运行，你希望终止这个进程吗？</translation>
    </message>
</context>
<context>
    <name>ToolbarBlock</name>
    <message>
        <location filename="../applications_toolbar.py" line="110"/>
        <source>Variable Selected</source>
        <translation type="unfinished">已选变量</translation>
    </message>
    <message>
        <location filename="../applications_toolbar.py" line="111"/>
        <source>No Variable</source>
        <translation type="unfinished">未选择变量</translation>
    </message>
</context>
</TS>
