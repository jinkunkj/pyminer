# 线程调用与后台任务执行器
示例可见：`pmgwidgets/examples/utilities/threadings`文件夹

>特别注意：
>
>在调用后台线程执行器PMGOneShot0hreadRunner\PMGLoopThreadRunner的时候，务必对执行器对象保持引用！
>
>否则执行器对象会被Python解释器垃圾回收，轻则任务无法完成，重则造成程序崩溃。
>
>为了简单起见，本文档中的实例都是面向过程的。在示例中将执行器对象定义为全局变量，就是保持引用的一种方式。而当使用面向对象程序设计方式时，将其作为界面或者控件的属性，也不失为一种可行的解决方案。

## 后台线程只执行一次
示例代码可以在`pmgwidgets/examples/utilities/threadings/singleshot.py`找到
```python
import sys
import time

from pmgwidgets import PMGOneShotThreadRunner  # 导入单线程任务运行器
from qtpy.QtWidgets import QTextEdit, QApplication


def run(loop_times):
    """
    任务函数。在函数执行之后，发出signal_finished事件，返回一个参数。如果函数有多个返回参数，则返回参数元组。
    :param loop_times: 
    :return: 
    """
    for i in range(loop_times):
        print(i)
        time.sleep(1)
    return 'finished!!', 'aaaaaa', ['finished', 123]


app = QApplication(sys.argv)
text = QTextEdit()
text.show()
oneshot = PMGOneShotThreadRunner(run, args=(5,))
oneshot.signal_finished.connect(lambda x: text.append('任务完成，函数返回值：' + repr(x)))
sys.exit(app.exec_())
```
### `PMGOneShotThreadRunner`类介绍
#### 传入参数：
- callback:传入函数对象。
- args: 传入函数的参数，默认值为None。应当以元组形式依次传入。如果为None则不对函数传入参数。
#### 信号
signal_finished，需要连接到的槽函数有一个参数，代表传入函数对象的返回值。

## 后台线程执行有限次
后台线程执行有限次的例子可见`pmgwidgets/examples/utilities/threadings/loop_limited_times1.py`与
`pmgwidgets/examples/utilities/threadings/loop_limited_times2.py`。
首先，执行有限次的模型是这样的：执行有限次同一函数，但是每次执行时都要传入不同的参数。这样，我们只要传入一个函数，以及一个参数列表，
列表长度就是循环的次数。返回的参数由`signal_step_finished`传回。
代码看这里：(`pmgwidgets/examples/utilities/threadings/loop_limited_times1.py`)
```python
import sys
import time
from qtpy.QtWidgets import QTextEdit, QApplication
from pmgwidgets import PMGLoopThreadRunner


def run(i, j):
    time.sleep(0.1)
    return i + j


def on_step_finished(step, result):
    global text1
    text1.append('传入每步不同的可迭代参数\nstep:%d,result:%s\n' % (step, repr(result)))


def on_finished():
    global text1
    text1.append('所有任务完成！')


app = QApplication(sys.argv)
text1 = QTextEdit()
text1.show()
oneshot = PMGLoopThreadRunner(run, iter_args=[(i, i + 1) for i in range(36)])
# 传入一个列表可迭代对象（当然也可以是其他迭代器。）作为参数。列表的长度就是循环的次数，列表的每一个元素代表每一步传入的参数。
oneshot.signal_step_finished.connect(on_step_finished)  # 每一步执行后的结果由signal_step_finished传回。多参数则会放进tuple里面。
oneshot.signal_finished.connect(on_finished)

sys.exit(app.exec_())
```
### `PMGLoopThreadRunner`类介绍
#### 传入参数：
- callback:传入函数对象。
- iter_args: 传入函数的参数，默认值为None。应当以可迭代对象的形式传入，可迭代对象的每个元素都是元组。可以为None。
**注意：只有当iter_args为None的时候，才会解析loop_times和step_args参数**！
- loop_times:执行的次数。
- step_args:当loop_times生效时每步执行传入的参数，类型应为元组。

通过以上的示例我们可以看出，loop_times的功能完全可以被iter_args替代。但为了简洁方便，还是设计了这一方法。
若要用loop_times配合step_args，则必须设置iter_args为None。
使用loop_times配合step_args的示例详见`pmgwidgets/examples/utilities/threadings/loop_limited_times2.py`
#### 信号
signal_step_finished:传递两个参数，分别是步数（整数，从0开始）和单步执行的结果。
signal_finished，意思后台线程执行完规定次数退出前发出的信号。它没有传递参数，在这点上与单步执行器不同。
这是因为在结束时，他只是通知一下前台程序”任务已经完成，后台线程要退出了“；而程序每一步的执行结果，都已经在signal_step_finished中传过了。