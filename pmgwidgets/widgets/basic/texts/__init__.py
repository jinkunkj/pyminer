try:
    from .editors import *
    from .lexers import *
except ModuleNotFoundError:
    pass