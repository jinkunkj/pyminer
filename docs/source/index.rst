PyMiner帮助文档
===================================

.. toctree::
   :maxdepth: 2

    主程序部分 <pyminer/__init__.rst>
    界面部分 <pmg/__init__.rst>
    算法部分 <alg/__init__.rst>
    插件部分 <pkgs/__init__.rst>

    参与贡献 <contribute/index.rst>

模块索引
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
