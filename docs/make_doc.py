import os

from pyminer2.dev.doc import Node, RstGenerator

if __name__ == '__main__':
    base = os.path.dirname(os.path.dirname(__file__))
    generator = RstGenerator()

    # 生成算法部分的文档
    py_dir = os.path.join(base, 'pyminer_algorithms')  # py源文件的目录
    rst_dir = os.path.join(base, 'docs/source/alg')  # 根据py生成的rst文件的目录
    exclude_dir = [os.path.join(py_dir, p) for p in (
        'plotting/graph.py',
    )]
    node = Node(py_dir, exclude_dir)
    generator.generate_rst(node, rst_dir, 'pyminer_algorithms')

    # 生成界面部分的文档
    py_dir = os.path.join(base, 'pmgwidgets')  # py源文件的目录
    rst_dir = os.path.join(base, 'docs/source/pmg')  # 根据py生成的rst文件的目录
    exclude_dir = [os.path.join(py_dir, p) for p in (
        'display/browser/get_ipy.py',
        'widgets/basic/texts/webeditors/__init__.py',
        'examples',
        'display/browser/handler.py',
        'widgets/basic/quick/demo1.py',
        'widgets/basic/texts',
        'widgets/extended/texts/texteditor.py',
        'utilities/platform/test',
    )]
    node = Node(py_dir, exclude_dir)
    generator.generate_rst(node, rst_dir, 'pmgwidgets')

    # 生成主程序的文档
    py_dir = os.path.join(base, 'pyminer2')  # py源文件的目录
    rst_dir = os.path.join(base, 'docs/source/pyminer')  # 根据py生成的rst文件的目录
    exclude_dir = [os.path.join(py_dir, p) for p in (
        'extensions/packages',
        'ui/pyqtsource_rc.py',
        'flowchart',
        'core',
        'extensions/package_manager/package_manager.py',
        'features/io/database.py',
    )]  # 排除插件文件夹
    node = Node(py_dir, exclude_dir)
    generator.generate_rst(node, rst_dir, 'pyminer2')

    # 生成插件的文档
    py_dir = os.path.join(base, 'pyminer2/extensions/packages')  # py源文件的目录
    rst_dir = os.path.join(base, 'docs/source/pkgs')  # 根据py生成的rst文件的目录
    exclude_dir = [os.path.join(py_dir, p) for p in (
        'pm_preprocess/ui/pyqtsource_rc.py',
        'code_editor',
        'pmagg',
        'data_miner',
        'document_server',
        'embedded-browser',
        'socket-server',
        'cftool',
        'extension_app_demo',
        'ipython_console',
        'pm_preprocess',
        'workspace_inspector',
    )]
    node = Node(py_dir, exclude_dir)
    generator.generate_rst(node, rst_dir, 'pyminer2.extensions.packages')

    for line in os.popen('"docs/make.bat" html'):
        print(line, end='')
