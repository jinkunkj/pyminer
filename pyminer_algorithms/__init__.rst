PyMiner 算法库
==============================================================================

.. toctree::
   :maxdepth: 2

   linear_algebra/__init__.rst

   plotting/__init__.rst

   pyminer_util/__init__.rst

   statistics/__init__.rst

